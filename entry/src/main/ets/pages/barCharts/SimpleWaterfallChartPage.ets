/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  ChartGesture,
  Description,
  EntryOhos,
  Highlight,
  JArrayList,
  Legend,
  LimitLabelPosition,
  LimitLine,
  MarkerView,
  OnChartGestureListener,
  OnChartValueSelectedListener,
  XAxis,
  XAxisPosition,
  YAxis,
  WaterfallDataSet,
  WaterfallEntry,
  WaterfallHighlight,
  WaterfallChart,
  WaterfallChartModel,
  IAxisValueFormatter,
  AxisBase,
  WaterfallData,
  IWaterfallDataSet
} from '@ohos/mpchart';
import title, { ChartTitleModel } from '../../title';
import { LogUtil } from '../../utils/LogUtil';

@Entry
@Component
struct SimpleWaterfallChartPage {
  private model: WaterfallChartModel | null = null;
  private dataSet: WaterfallDataSet | null = null;
  private leftAxis: YAxis | null = null;
  private rightAxis: YAxis | null = null;
  private xAxis: XAxis | null = null;
  private limitLine1: LimitLine | null = null;
  private limitLine2: LimitLine | null = null;
  private data: WaterfallData | null = null;
  private normalMarker: MarkerView | null = null;
  //标题栏标题
  private title: string = 'Waterfall'
  @State titleModel: ChartTitleModel = new ChartTitleModel()
  private valueSelectedListener: OnChartValueSelectedListener = {
    onValueSelected: (e: EntryOhos, h: Highlight) => {
      LogUtil.info("SimpleWaterfallChartPage onValueSelected: " + e.getX());
    },
    onNothingSelected: () => {
      LogUtil.info("SimpleWaterfallChartPage onNothingSelected");
    }
  }
  private chartGestureListener: OnChartGestureListener = {
    onChartGestureStart: (isTouchEvent: boolean, me: TouchEvent | GestureEvent, lastPerformedGestureMode: ChartGesture) => {
      LogUtil.info("-----------------chartGestureListener onChartGestureStart lastMode: " + lastPerformedGestureMode);
    },
    onChartGestureEnd: (isTouchEvent: boolean, me: TouchEvent | GestureEvent, lastPerformedGestureMode: ChartGesture) => {
      LogUtil.info("-----------------chartGestureListener onChartGestureEnd lastMode: " + lastPerformedGestureMode);
    },
    onChartLongPressed: (isTouchEvent: boolean, me: TouchEvent | GestureEvent) => {
      LogUtil.info("-----------------chartGestureListener onChartLongPressed");
    },
    onChartDoubleTapped: (isTouchEvent: boolean, me: TouchEvent | GestureEvent) => {
      LogUtil.info("-----------------chartGestureListener onChartDoubleTapped");
    },
    onChartSingleTapped: (isTouchEvent: boolean, me: TouchEvent | GestureEvent) => {
      LogUtil.info("-----------------chartGestureListener onChartSingleTapped");
    },
    onChartFling: (isTouchEvent: boolean, me1: TouchEvent | GestureEvent, me2: TouchEvent, velocityX: number, velocityY: number) => {
      LogUtil.info("-----------------chartGestureListener onChartFling velocityX: " + velocityX + "  velocityY: " + velocityY);
    },
    onChartScale: (isTouchEvent: boolean, me: TouchEvent | GestureEvent, scaleX: number, scaleY: number) => {
      LogUtil.info("-----------------chartGestureListener onChartScale scaleX: " + scaleX + "  scaleY: " + scaleY);
    },
    onChartTranslate: (isTouchEvent: boolean, me: TouchEvent | GestureEvent, dX: number, dY: number) => {
      LogUtil.info("-----------------chartGestureListener onChartTranslate dx: " + dX + "  dy: " + dY);
    }
  }

  aboutToAppear() {
    this.titleModel.title = this.title
    this.model = new WaterfallChartModel();
    this.model.setOnChartValueSelectedListener(this.valueSelectedListener);
    this.model.setOnChartGestureListener(this.chartGestureListener);

    let description: Description | null = this.model.getDescription()
    if (description) {
      description.setEnabled(false);
    }

    let l: Legend | null = this.model.getLegend();
    if (l) {
      l.setEnabled(false);
    }

    // if more than 40 entries are displayed in the this.model, no values will be drawn
    this.model.setMaxVisibleValueCount(40);

    // scaling can now only be done on x- and y-axis separately
    this.model.setPinchZoom(false);

    this.model.setDrawGridBackground(false);
    this.model.setGridBackgroundColor('#500000ff')
    this.model.setDrawBarShadow(false);

    this.model.setDrawValueAboveBar(false);
    this.model.setHighlightFullBarEnabled(false);

    this.limitLine1 = new LimitLine(120, 'Upper Limit');
    this.limitLine1.setLineWidth(4);
    this.limitLine1.enableDashedLine(10, 10, 0);
    this.limitLine1.setLabelPosition(LimitLabelPosition.RIGHT_TOP);
    this.limitLine1.setTextSize(10);

    this.limitLine2 = new LimitLine(50, 'Lower Limit');
    this.limitLine2.setLineWidth(4);
    this.limitLine2.enableDashedLine(10, 10, 0);
    this.limitLine2.setLineColor(Color.Yellow);
    this.limitLine2.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
    this.limitLine2.setTextSize(10);

    // change the position of the y-labels
    this.leftAxis = this.model.getAxisLeft();
    if (this.leftAxis) {
      this.leftAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
      this.leftAxis.setDrawLimitLinesBehindData(false);
      this.leftAxis.setValueFormatter(new CustomFormatter());

      // add limit lines
      this.leftAxis.addLimitLine(this.limitLine1);
      this.leftAxis.addLimitLine(this.limitLine2);
    }

    this.rightAxis = this.model.getAxisRight();
    if (this.rightAxis) {
      this.rightAxis.setEnabled(false);
      this.rightAxis.setAxisMinimum(0);
    }

    this.xAxis = this.model.getXAxis();
    if (this.xAxis) {
      this.xAxis.setPosition(XAxisPosition.BOTTOM);
    }

    this.normalMarker = new MarkerView();
    this.model.setMarker(this.normalMarker);

    this.data = this.getNormalData();
    this.model.setData(this.data);
    this.model.setVisibleXRangeMaximum(5);
  }

  private getNormalData(): WaterfallData {
    let values: JArrayList<WaterfallEntry> = new JArrayList<WaterfallEntry>();
    let h1 = new WaterfallHighlight(20, 30, Color.Gray);
    let h2 = new WaterfallHighlight(30, 40, Color.Gray);
    let h3 = new WaterfallHighlight(40, 60, Color.Green);
    let h4 = new WaterfallHighlight(60, 70, Color.Red);

    values.add(new WaterfallEntry(1, 10, 30, undefined, undefined, h1));
    values.add(new WaterfallEntry(2, 15, 50));
    values.add(new WaterfallEntry(3, 20, 90));
    values.add(new WaterfallEntry(4, 5, 95, undefined, undefined, h2, h3, h4));
    values.add(new WaterfallEntry(5, 5, 50));
    values.add(new WaterfallEntry(6, 45, 80, undefined, undefined, h2, h3));
    values.add(new WaterfallEntry(7, 60, 70));

    this.dataSet = new WaterfallDataSet(values, 'DataSet');
    this.dataSet.setHighLightColor(Color.Gray);
    this.dataSet.setDrawIcons(false);
    this.dataSet.setColorByColor(Color.Pink);
    this.dataSet.setValueTextSize(10)

    let dataSetList: JArrayList<IWaterfallDataSet> = new JArrayList<IWaterfallDataSet>();
    dataSetList.add(this.dataSet);

    let data: WaterfallData = new WaterfallData(dataSetList);
    data.setBarWidth(0.3);
    return data;
  }

  private getNormalData2(): WaterfallData {
    let values: JArrayList<WaterfallEntry> = new JArrayList<WaterfallEntry>();

    values.add(new WaterfallEntry(1, 10, 70));
    values.add(new WaterfallEntry(2, 15, 80));
    values.add(new WaterfallEntry(3, 20, 90));
    values.add(new WaterfallEntry(4, 5, 80));
    values.add(new WaterfallEntry(5, 5, 50));
    values.add(new WaterfallEntry(6, 45, 80));
    values.add(new WaterfallEntry(7, 60, 70));


    this.dataSet = new WaterfallDataSet(values, 'DataSet');
    this.dataSet.setHighLightColor(Color.Gray);
    this.dataSet.setDrawIcons(false);
    this.dataSet.setColorByColor(Color.Pink);
    this.dataSet.setValueTextSize(10)

    // 根据Y刻度范围设置颜色
    let highlightList: WaterfallHighlight[] = [];
    highlightList.push(new WaterfallHighlight(0, 40, Color.Green));
    highlightList.push(new WaterfallHighlight(40, 60, Color.Orange));
    highlightList.push(new WaterfallHighlight(60, 100, Color.Pink));
    this.dataSet.setYAxisSegmentationColors(highlightList);

    let dataSetList: JArrayList<IWaterfallDataSet> = new JArrayList<IWaterfallDataSet>();
    dataSetList.add(this.dataSet);

    let data: WaterfallData = new WaterfallData(dataSetList);
    data.setBarWidth(0.3);
    return data;
  }

  private getNormalData3(): WaterfallData {
    let values: JArrayList<WaterfallEntry> = new JArrayList<WaterfallEntry>();

    values.add(new WaterfallEntry(1, 10, 90));
    values.add(new WaterfallEntry(2, 15, 80));
    values.add(new WaterfallEntry(3, 20, 90));
    values.add(new WaterfallEntry(4, 5, 80));
    values.add(new WaterfallEntry(5, 5, 50));
    values.add(new WaterfallEntry(6, 45, 80));
    values.add(new WaterfallEntry(7, 60, 70));

    this.dataSet = new WaterfallDataSet(values, 'DataSet');
    this.dataSet.setHighLightColor(Color.Gray);
    this.dataSet.setDrawIcons(false);
    this.dataSet.setColorByColor(Color.Pink);
    this.dataSet.setValueTextSize(10)
    // 根据Y刻度范围设置颜色
    let highlightList: WaterfallHighlight[] = [];
    highlightList.push(new WaterfallHighlight(0, 40, '#cddc39'));
    highlightList.push(new WaterfallHighlight(40, 60, '#00bcd4'));
    highlightList.push(new WaterfallHighlight(60, 100, '#ff9800'));
    this.dataSet.setYAxisSegmentationColors(highlightList);
    // 最高点最低点高亮时颜色设置
    this.dataSet.setEnableMaxOrMinHighlightColor(true);
    this.dataSet.setMaxyHighlightColor(Color.Brown);
    this.dataSet.setMinyHighlightColor(Color.Yellow);
    let dataSetList: JArrayList<IWaterfallDataSet> = new JArrayList<IWaterfallDataSet>();
    dataSetList.add(this.dataSet);

    let data: WaterfallData = new WaterfallData(dataSetList);
    data.setBarWidth(0.3);
    return data;
  }

  build() {
    Column() {
      title({ model: this.titleModel })
      Column() {
        WaterfallChart({ model: this.model })
          .width('100%')
          .height('70%')
      }
      Row() {
        Row() {
          Text('普通: ')
          Radio({ value: 'CUBIC_BEZIER', group: 'dataType' })
            .checked(true)
            .onChange((isChecked: boolean) => {
              if (isChecked) {
                this.data = this.getNormalData()
                if (this.dataSet && this.model) {
                  this.model.setData(this.data);
                }
              }
            })
        }

        Row() {
          Text('通过Y轴分段: ')
          Radio({ value: 'CUBIC_BEZIER', group: 'dataType' })
            .checked(false)
            .onChange((isChecked: boolean) => {
              if (isChecked) {
                this.data = this.getNormalData2()
                if (this.dataSet && this.model) {
                  this.model.setData(this.data);
                }
              }
            })
        }
        Row() {
          Text('最高最低点高亮: ')
          Radio({ value: 'CUBIC_BEZIER', group: 'dataType' })
            .checked(false)
            .onChange((isChecked: boolean) => {
              if (isChecked) {
                this.data = this.getNormalData3()
                if (this.dataSet && this.model) {
                  this.model.setData(this.data);
                }
              }
            })
        }
      }.alignSelf(ItemAlign.Center)
    }
  }
}

class CustomFormatter implements IAxisValueFormatter {
  getFormattedValue(value: number, axis: AxisBase): string {
    return Math.abs(value).toFixed(0) + "%";
  }
}